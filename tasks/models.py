from django.db import models
from django.conf import settings


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField(auto_now_add=False)
    is_completed = models.BooleanField(default=False)  # look up default values
    project = models.ForeignKey(
        "projects.Project",  # project.Projects
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
